﻿namespace StarWars.Core.Validator
{
    public interface IValidatable
    {
        /// <summary>
        /// Checks if object is valid. If not it will throw HttpErrorException
        /// </summary>
        void Validate();
    }
}