﻿using System;
using System.Net;

namespace StarWars.Core.Error
{
    public class HttpErrorException : Exception
    {
        public HttpErrorException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}