﻿using System.Net;

namespace StarWars.Core.Error
{
    public static class HttpError
    {
        //400
        public static HttpErrorException BadRequest(string message) =>
            new HttpErrorException(HttpStatusCode.BadRequest, message);

        //401
        public static HttpErrorException Unauthorized(string message) =>
            new HttpErrorException(HttpStatusCode.Unauthorized, message);

        //403
        public static HttpErrorException Forbidden(string message) =>
            new HttpErrorException(HttpStatusCode.Forbidden, message);

        //404
        public static HttpErrorException NotFound(string message) =>
            new HttpErrorException(HttpStatusCode.NotFound, message);

        //500
        public static HttpErrorException InternalServerError(string message) =>
            new HttpErrorException(HttpStatusCode.InternalServerError, message);
    }
}