﻿using Microsoft.EntityFrameworkCore;
using StarWars.Context.Entities;

namespace StarWars.Context
{
    public class StarWarsContext : DbContext
    {
        protected StarWarsContext()
        {
        }

        public StarWarsContext(DbContextOptions<StarWarsContext> options) : base(options)
        {
        }


        public DbSet<CharacterEntity> Characters { get; set; }
        public DbSet<EpisodeEntity> Episodes { get; set; }
        public DbSet<CharacterFriendEntity> CharacterFriends { get; set; }
        public DbSet<CharacterEpisodeEntity> CharacterEpisodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region CharacterEntity

            modelBuilder.Entity<CharacterEntity>()
               .HasKey(c => c.Id);

            modelBuilder.Entity<CharacterEntity>()
               .Property(e => e.Name)
               .IsRequired();

            #endregion

            #region EpisodeEntity

            modelBuilder.Entity<EpisodeEntity>()
               .HasKey(e => e.Id);

            modelBuilder.Entity<EpisodeEntity>()
               .Property(e => e.Name)
               .IsRequired();

            #endregion

            #region CharacterEntity-EpisodeEntity many to many

            modelBuilder.Entity<CharacterEpisodeEntity>()
               .HasKey(e => new {e.CharacterId, e.EpisodeId});

            modelBuilder.Entity<CharacterEpisodeEntity>()
               .HasOne(e => e.Character)
               .WithMany(c => c.CharacterEpisodes)
               .HasForeignKey(e => e.CharacterId);

            modelBuilder.Entity<CharacterEpisodeEntity>()
               .HasOne(e => e.Episode)
               .WithMany(ep => ep.CharacterEpisodes)
               .HasForeignKey(e => e.EpisodeId);

            #endregion

            #region CharacterFriend many to many

            modelBuilder.Entity<CharacterFriendEntity>()
               .HasKey(e => new {e.CharacterId, e.FriendId});

            modelBuilder.Entity<CharacterFriendEntity>()
               .HasOne(pt => pt.Friend)
               .WithMany(p => p.FriendOf)
               .HasForeignKey(pt => pt.FriendId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CharacterFriendEntity>()
               .HasOne(pt => pt.Character)
               .WithMany(t => t.Friends)
               .HasForeignKey(pt => pt.CharacterId);

            #endregion
        }
    }
}