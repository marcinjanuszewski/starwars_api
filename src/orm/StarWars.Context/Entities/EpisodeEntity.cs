﻿using System.Collections.Generic;

namespace StarWars.Context.Entities
{
    public class EpisodeEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<CharacterEpisodeEntity> CharacterEpisodes { get; set; }
    }
}