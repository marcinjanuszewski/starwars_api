﻿using System.Collections.Generic;

namespace StarWars.Context.Entities
{
    public class CharacterEntity
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Planet { get; set; }

        public virtual IList<CharacterEpisodeEntity> CharacterEpisodes { get; set; }
        public virtual IList<CharacterFriendEntity> Friends { get; set; }
        public virtual IList<CharacterFriendEntity> FriendOf { get; set; }
    }
}