﻿namespace StarWars.Context.Entities
{
    public class CharacterEpisodeEntity
    {
        public string CharacterId { get; set; }
        public CharacterEntity Character { get; set; }
        public string EpisodeId { get; set; }
        public EpisodeEntity Episode { get; set; }
    }
}