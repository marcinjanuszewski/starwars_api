﻿namespace StarWars.Context.Entities
{
    public class CharacterFriendEntity
    {
        public string CharacterId { get; set; }
        public CharacterEntity Character { get; set; }
        public string FriendId { get; set; }
        public CharacterEntity Friend { get; set; }
    }
}