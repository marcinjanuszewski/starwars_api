﻿namespace StarWars.Domain.Query
{
    public class CharacterSearchQuery : BaseSearchQuery
    {
        public string Name { get; set; }
        public string Planet { get; set; }
    }
}