﻿namespace StarWars.Domain.Query
{
    public class EpisodeSearchQuery : BaseSearchQuery
    {
        public string Name { get; set; }
    }
}