﻿using StarWars.Core.Error;
using StarWars.Core.Validator;

namespace StarWars.Domain.Query
{
    public class BaseSearchQuery : IValidatable
    {
        public int Page { get; set; } = 1;
        public int Count { get; set; } = 50;

        ///<inheritdoc/>
        public void Validate()
        {
            if (Page < 1)
                throw HttpError.BadRequest("Page must be greater or equal than 1");

            if (Count < 1)
                throw HttpError.BadRequest("Count must be greater or equal than 1");
        }
    }
}