﻿using StarWars.Domain.Model;

namespace StarWars.Domain.Pagination
{
    public class Paged<T> where T : Resource
    {
        public int Total { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public T[] Resources { get; set; }
    }
}