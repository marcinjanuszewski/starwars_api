﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarWars.Domain.Model
{
    public class Character : Resource
    {
        public string Name { get; set; }
        public string Planet { get; set; }
        public string[] FriendsIds { get; set; } = new string[0];
        public string[] EpisodesIds { get; set; } = new string[0];
    }
}
