﻿namespace StarWars.Domain.Model
{
    public abstract class Resource
    {
        public string Id { get; set; }
    }
}