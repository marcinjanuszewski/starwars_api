﻿using System.Threading.Tasks;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Episode;

namespace StarWars.Services.Episode
{
    public class EpisodeService : IEpisodeService
    {
        private readonly IEpisodeRepository _episodeRepository;

        public EpisodeService(IEpisodeRepository episodeRepository)
        {
            _episodeRepository = episodeRepository;
        }

        public async Task<Domain.Model.Episode> CreateAsync(Domain.Model.Episode episode)
        {
            if (episode == null)
                throw HttpError.BadRequest("Episode must be provided during Create operation.");

            return await _episodeRepository.CreateAsync(episode);
        }

        public async Task<Domain.Model.Episode> ReadAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Read operation");

            return await _episodeRepository.ReadAsync(id);
        }


        public async Task<Domain.Model.Episode> UpdateAsync(Domain.Model.Episode episode)
        {
            if (episode == null)
                throw HttpError.BadRequest("Episode must be provided during Update operation.");

            if (string.IsNullOrEmpty(episode.Id))
                throw HttpError.BadRequest("Episode.id must be provided during Update operation.");

            return await _episodeRepository.UpdateAsync(episode);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Delete operation");

            return await _episodeRepository.DeleteAsync(id);
        }

        public async Task<Paged<Domain.Model.Episode>> SearchAsync(EpisodeSearchQuery query) =>
            await _episodeRepository.SearchAsync(query ?? new EpisodeSearchQuery());
    }
}