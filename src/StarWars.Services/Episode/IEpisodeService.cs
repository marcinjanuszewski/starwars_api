﻿using System.Threading.Tasks;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;

namespace StarWars.Services.Episode
{
    public interface IEpisodeService : ICrudService<Domain.Model.Episode>
    {
        Task<Paged<Domain.Model.Episode>> SearchAsync(EpisodeSearchQuery query);
    }
}