﻿using System.Threading.Tasks;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;

namespace StarWars.Services.Character
{
    public interface ICharacterService : ICrudService<Domain.Model.Character>
    {
        Task<Paged<Domain.Model.Character>> SearchAsync(CharacterSearchQuery query);
    }
}