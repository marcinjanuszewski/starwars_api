﻿using System.Threading.Tasks;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Character;

namespace StarWars.Services.Character
{
    public class CharacterService : ICharacterService
    {
        private readonly ICharacterRepository _characterRepository;

        public CharacterService(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public async Task<Domain.Model.Character> CreateAsync(Domain.Model.Character character)
        {
            if (character == null)
                throw HttpError.BadRequest("Character must be provided during Create operation.");

            return await _characterRepository.CreateAsync(character);
        }

        public async Task<Domain.Model.Character> ReadAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Read operation");

            return await _characterRepository.ReadAsync(id);
        }


        public async Task<Domain.Model.Character> UpdateAsync(Domain.Model.Character character)
        {
            if (character == null)
                throw HttpError.BadRequest("Character must be provided during Update operation.");

            if (string.IsNullOrEmpty(character.Id))
                throw HttpError.BadRequest("Character.id must be provided during Update operation.");

            return await _characterRepository.UpdateAsync(character);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Delete operation");

            return await _characterRepository.DeleteAsync(id);
        }

        public async Task<Paged<Domain.Model.Character>> SearchAsync(CharacterSearchQuery query) =>
            await _characterRepository.SearchAsync(query ?? new CharacterSearchQuery());
    }
}