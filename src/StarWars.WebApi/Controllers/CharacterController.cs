﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StarWars.Domain.Model;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Services.Character;

namespace StarWars.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;

        public CharacterController(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Character), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> ReadAsync(string id)
        {
            Character result = await _characterService.ReadAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(typeof(Paged<Character>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> SearchAsync([FromQuery] CharacterSearchQuery query)
        {
            query = query ?? new CharacterSearchQuery();
            return Ok(await _characterService.SearchAsync(query));
        }


        [HttpPost]
        [ProducesResponseType(typeof(Character), (int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] Character character)
        {
            if (character == null)
                return BadRequest("Body must not be null.");

            return Ok(await _characterService.CreateAsync(character));
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(Character), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateAsync(string id, [FromBody] Character character)
        {
            if (character == null)
                return BadRequest("Body must not be null.");

            if (character.Id != id)
                return BadRequest("Mismatch between character Id from route and body.");

            return Ok(await _characterService.UpdateAsync(character));
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            bool result = await _characterService.DeleteAsync(id);
            return result ? NoContent() : StatusCode(500);
        }
    }
}