﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StarWars.Domain.Model;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Services.Episode;

namespace StarWars.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EpisodeController : ControllerBase
    {
        private readonly IEpisodeService _episodeService;

        public EpisodeController(IEpisodeService episodeService)
        {
            _episodeService = episodeService;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Episode), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> ReadAsync(string id)
        {
            var result = await _episodeService.ReadAsync(id);
            if (result == null)
                return NotFound();
            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(typeof(Paged<Episode>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> SearchAsync([FromQuery] EpisodeSearchQuery query)
        {
            query = query ?? new EpisodeSearchQuery();
            return Ok(await _episodeService.SearchAsync(query));
        }


        [HttpPost]
        [ProducesResponseType(typeof(Episode), (int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] Episode episode)
        {
            if(episode == null)
                return BadRequest("Body must not be null.");

            return Ok(await _episodeService.CreateAsync(episode));
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(Episode), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateAsync(string id, [FromBody] Episode episode)
        {
            if (episode == null)
                return BadRequest("Body must not be null.");

            if (episode.Id != id)
                return BadRequest("Mismatch between episode Id from route and body.");

            return Ok(await _episodeService.UpdateAsync(episode));
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            bool result = await _episodeService.DeleteAsync(id);
            return result ? NoContent() : StatusCode(500);
        }
    }
}