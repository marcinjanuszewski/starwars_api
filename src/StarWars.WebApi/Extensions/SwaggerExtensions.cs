﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWars.WebApi.Extensions
{
    public static class SwaggerExtensions
    {
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {
            return services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "StarWars API",
                        Version = "v1"
                    });

                //Include documentation from XMLs
                c.IncludeComments();
            });
        }

        public static IApplicationBuilder ConfigureSwagger(this IApplicationBuilder app)
        {
            return app.UseSwagger()
               .UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "StarWars API V1"); });
        }

        private static void IncludeComments(this SwaggerGenOptions options)
        {
            string[] documentationXmls = AppDomain.CurrentDomain.GetAssemblies()
               .Where(x => x.FullName.StartsWith("StarWars."))
               .Select(x => $"{x.FullName.Split(',')[0]}.xml")
               .Select(x => Path.Combine(AppContext.BaseDirectory, x))
               .Where(File.Exists)
               .ToArray();

            foreach (string xml in documentationXmls)
                options.IncludeXmlComments(xml);
        }
    }
}