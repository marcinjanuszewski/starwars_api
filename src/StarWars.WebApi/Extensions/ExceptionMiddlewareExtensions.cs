﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StarWars.Core.Error;

namespace StarWars.WebApi.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            ILogger logger = loggerFactory.CreateLogger("ExceptionHandlerLogger");
            app.UseExceptionHandler(error =>
            {
                error.Run(async context =>
                {
                    context.Response.ContentType = "application/json";
                    Exception exception = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
                    if (exception != null)
                    {
                        string guid = Guid.NewGuid().ToString();
                        logger.LogError(exception,
                            $"An error ({guid}) occurred during request: {context.Request.Path}");
                        if (exception is HttpErrorException errorException)
                            await context.HandleException(errorException);
                        else
                            await context.HandleException(guid);
                    }
                });
            });
        }

        private static async Task HandleException(this HttpContext context, HttpErrorException exception)
        {
            await context.HandleException((int) exception.StatusCode, exception.Message, "text/plain");
        }

        private static async Task HandleException(this HttpContext context, string guid)
        {
            await context.HandleException((int) HttpStatusCode.InternalServerError,
                $"An error (id: {guid}) occurred.",
                "text/plain");
        }

        private static async Task HandleException(
            this HttpContext context,
            int statusCode,
            string message,
            string contentType)
        {
            context.Response.StatusCode = statusCode;
            context.Response.ContentType = contentType;
            await context.Response.WriteAsync(message);
        }
    }
}