﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StarWars.Context;

namespace StarWars.WebApi.Extensions
{
    public static class DatabaseExtensions
    {
        public static void ConfigureStarWarsDbContext(this IServiceCollection s, IConfiguration appSettings)
        {
            string db = appSettings.GetSection("Database")?.Value;
            if (db == "Postgres")
                s.AddEntityFrameworkNpgsql().AddDbContext<StarWarsContext>(o =>
                    o.UseNpgsql(appSettings.GetConnectionString("Postgres"),
                        npgo => npgo.MigrationsAssembly("StarWars.Migrations.Postgres")));
            else
                throw new Exception($"Database {db} not supported");

            s.EnsureMigrated();
        }


        private static void EnsureMigrated(this IServiceCollection s)
        {
            ServiceProvider sp = s.BuildServiceProvider();
            var context = sp.GetService<StarWarsContext>();

            context.Database.Migrate();
        }
    }
}