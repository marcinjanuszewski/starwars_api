﻿using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StarWars.Repository.Character;
using StarWars.Repository.Episode;
using StarWars.Services.Character;
using StarWars.Services.Episode;

namespace StarWars.WebApi.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection ConfigureServices(
            this IServiceCollection services,
            IConfiguration configuration) =>
            services
               .AddScoped<ICharacterRepository>(configuration.GetValue<string>("CharacterRepositoryType"))
               .AddScoped<IEpisodeRepository>(configuration.GetValue<string>("EpisodeRepositoryType"))
               .AddScoped<ICharacterService, CharacterService>()
               .AddScoped<IEpisodeService, EpisodeService>();




        private static IServiceCollection AddScoped<TInterface>(this IServiceCollection services, string implementationTypeName)
        {
            var implementation = typeof(TInterface).Assembly
               .GetTypes()
               .FirstOrDefault(x => typeof(TInterface).IsAssignableFrom(x) && x.Name == implementationTypeName);
            return services.AddScoped(typeof(TInterface), implementation);
        }
    }
}