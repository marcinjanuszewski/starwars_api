﻿using System;
using System.Collections.Generic;
using System.Linq;
using StarWars.Context.Entities;

namespace StarWars.Repository.Mapper
{
    internal static class CharacterMapper
    {
        internal static Domain.Model.Character MapToCharacter(this CharacterEntity entity)
        {
            var character = new Domain.Model.Character
            {
                Id = entity.Id,
                Name = entity.Name,
                Planet = entity.Planet
            };


            List<string> friendIds = new List<string>();
            if (entity.Friends?.Any() == true)
                friendIds.AddRange(entity.Friends.Select(x => x.FriendId));

            if (entity.FriendOf?.Any() == true)
                friendIds.AddRange(entity.FriendOf.Select(x => x.CharacterId));

            character.FriendsIds = friendIds.Distinct().Where(x => x != entity.Id).ToArray();
            character.EpisodesIds = entity.CharacterEpisodes?.Select(x => x.EpisodeId).Distinct().ToArray();

            return character;
        }


        internal static CharacterEntity MapToCharacterEntity(this Domain.Model.Character character)
        {
            var entity = new CharacterEntity
            {
                Id = character.Id ?? Guid.NewGuid().ToString(),
                Name = character.Name,
                Planet = character.Planet
            };

            entity.CharacterEpisodes = character.EpisodesIds?
               .Select(x => new CharacterEpisodeEntity {CharacterId = entity.Id, EpisodeId = x})
               .ToArray();

            entity.Friends = character.FriendsIds?
               .Select(x => new CharacterFriendEntity {CharacterId = entity.Id, FriendId = x})
               .ToArray();

            return entity;
        }
    }
}