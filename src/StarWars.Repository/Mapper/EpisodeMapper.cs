﻿using System.Collections.Generic;
using StarWars.Context.Entities;

namespace StarWars.Repository.Mapper
{
    internal static class EpisodeMapper
    {
        internal static Domain.Model.Episode MapToEpisode(this EpisodeEntity entity) =>
            new Domain.Model.Episode
            {
                Id = entity.Id,
                Name = entity.Name
            };


        internal static EpisodeEntity MapToEpisodeEntity(this Domain.Model.Episode episode) =>
            new EpisodeEntity
            {
                Id = episode.Id,
                Name = episode.Name,
                CharacterEpisodes = new List<CharacterEpisodeEntity>()
            };
    }
}