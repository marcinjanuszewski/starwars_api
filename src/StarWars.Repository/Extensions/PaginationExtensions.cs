﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StarWars.Domain.Model;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;

namespace StarWars.Repository.Extensions
{
    public static class PaginationExtensions
    {
        public static Paged<T> Paginate<T>(this T[] resources, BaseSearchQuery query) where T : Resource =>
            new Paged<T>
            {
                Page = query.Page,
                PageSize = query.Count,
                Total = resources.Length,
                Resources = resources.Skip((query.Page - 1) * query.Count).Take(query.Count).ToArray()
            };


        public static async Task<Paged<TModel>> Paginate<TEntity, TModel>(
            this IQueryable<TEntity> queryable,
            BaseSearchQuery query,
            Func<TEntity, TModel> mapperFunc) where TModel : Resource
        {
            int entitiesCount = queryable.Count();

            TEntity[] entities = await queryable
               .Skip((query.Page - 1) * query.Count)
               .Take(query.Count)
               .ToArrayAsync();

            return new Paged<TModel>
            {
                Page = query.Page,
                PageSize = query.Count,
                Total = entitiesCount,
                Resources = entities.Select(mapperFunc).ToArray()
            };
        }
    }
}