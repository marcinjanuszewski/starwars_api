﻿using System.Threading.Tasks;
using StarWars.Domain.Model;

namespace StarWars.Repository
{
    public interface ICrudRepository<T> where T : Resource
    {
        Task<T> CreateAsync(T resource);
        Task<T> ReadAsync(string id);
        Task<T> UpdateAsync(T resource);
        Task<bool> DeleteAsync(string id);
    }
}