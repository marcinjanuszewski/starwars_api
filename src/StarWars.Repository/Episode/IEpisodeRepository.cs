﻿using System.Threading.Tasks;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;

namespace StarWars.Repository.Episode
{
    public interface IEpisodeRepository : ICrudRepository<Domain.Model.Episode>
    {
        Task<Paged<Domain.Model.Episode>> SearchAsync(EpisodeSearchQuery query);
    }
}