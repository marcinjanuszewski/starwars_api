﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Extensions;

namespace StarWars.Repository.Episode
{
    public class MockEpisodeRepository : IEpisodeRepository
    {
        private static readonly Domain.Model.Episode[] _episodes =
        {
            new Domain.Model.Episode
            {
                Id = "b0f4f0b3-b1ea-4bd6-9510-6ec2b78e2aaa",
                Name = "EMPIRE"
            },
            new Domain.Model.Episode
            {
                Id = "e4ec115e-080a-41a7-aaa8-2200b3a95b28",
                Name = "JEDI"
            },
            new Domain.Model.Episode
            {
                Id = "e17e772f-62c4-471c-8c0d-71e9a7801445",
                Name = "NEWHOPE"
            }
        };

        public Task<Domain.Model.Episode> CreateAsync(Domain.Model.Episode episode) =>
            throw HttpError.InternalServerError("Not implemented.");

        public Task<Domain.Model.Episode> ReadAsync(string id) =>
            Task.FromResult(_episodes.FirstOrDefault(x => x.Id == id));

        public Task<Domain.Model.Episode> UpdateAsync(Domain.Model.Episode character) =>
            throw HttpError.InternalServerError("Not implemented.");

        public Task<bool> DeleteAsync(string id) => throw HttpError.InternalServerError("Not implemented.");

        public Task<Paged<Domain.Model.Episode>> SearchAsync(EpisodeSearchQuery query)
        {
            IEnumerable<Domain.Model.Episode> episodes = _episodes;
            if (!string.IsNullOrEmpty(query.Name))
                episodes = episodes.Where(x => x.Name == query.Name);

            Domain.Model.Episode[] filteredEpisodes = episodes.ToArray();

            return Task.FromResult(filteredEpisodes.Paginate(query));
        }
    }
}