﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using StarWars.Context;
using StarWars.Context.Entities;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Extensions;
using StarWars.Repository.Mapper;

namespace StarWars.Repository.Episode
{
    public class DbEpisodeRepository : IEpisodeRepository
    {
        private readonly StarWarsContext _context;

        public DbEpisodeRepository(StarWarsContext context)
        {
            _context = context;
        }

        public async Task<Domain.Model.Episode> CreateAsync(Domain.Model.Episode episode)
        {
            if (episode == null)
                throw HttpError.BadRequest("Episode must not be null.");

            episode.Id = Guid.NewGuid().ToString();

            EntityEntry<EpisodeEntity> result = await _context.Episodes.AddAsync(episode.MapToEpisodeEntity());

            if (await _context.SaveChangesAsync() > 0)
                return result.Entity.MapToEpisode();

            throw HttpError.InternalServerError("An error occurred during providing episode to database");
        }

        public async Task<Domain.Model.Episode> ReadAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Read operation");

            EpisodeEntity entity = await GetQueryable().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return entity?.MapToEpisode();
        }

        public async Task<Domain.Model.Episode> UpdateAsync(Domain.Model.Episode episode)
        {
            if (episode == null)
                throw HttpError.BadRequest("Episode must not be null.");

            if (string.IsNullOrEmpty(episode.Id))
                throw HttpError.BadRequest("Episode.id must not be null.");

            EpisodeEntity entity = await GetQueryable()
               .FirstOrDefaultAsync(x => x.Id == episode.Id);
            if (entity == null)
                throw HttpError.NotFound($"Episode ({episode.Id}) does not exist");

            entity.Name = episode.Name;

            EntityEntry<EpisodeEntity> result = _context.Episodes.Update(entity);
            if (await _context.SaveChangesAsync() > 0)
                return result.Entity.MapToEpisode();

            throw HttpError.InternalServerError("An error occurred during providing episode to database");
        }

        public async Task<bool> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Delete operation");

            EpisodeEntity entity = await GetQueryable().FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
                throw HttpError.NotFound($"Episode ({id}) does not exist");

            if (entity.CharacterEpisodes.Any())
                throw HttpError.BadRequest($"Episode ({id}) has some character references. Remove them first.");

            _context.Episodes.Remove(entity);

            return await _context.SaveChangesAsync() > 0;
        }

        public Task<Paged<Domain.Model.Episode>> SearchAsync(EpisodeSearchQuery query)
        {
            if (query == null)
                throw HttpError.BadRequest("Search action must have query");
            
            query.Validate();

            IQueryable<EpisodeEntity> queryable = GetQueryable().AsNoTracking();

            if (!string.IsNullOrEmpty(query.Name))
                queryable = queryable.Where(x => x.Name == query.Name);

            return queryable.Paginate(query, entity => entity.MapToEpisode());
        }


        private IQueryable<EpisodeEntity> GetQueryable() => _context.Episodes
           .Include(x => x.CharacterEpisodes);
    }
}