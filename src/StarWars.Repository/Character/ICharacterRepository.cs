﻿using System.Threading.Tasks;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;

namespace StarWars.Repository.Character
{
    public interface ICharacterRepository : ICrudRepository<Domain.Model.Character>
    {
        Task<Paged<Domain.Model.Character>> SearchAsync(CharacterSearchQuery query);
    }
}