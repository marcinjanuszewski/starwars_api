﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using StarWars.Context;
using StarWars.Context.Entities;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Extensions;
using StarWars.Repository.Mapper;

namespace StarWars.Repository.Character
{
    public class DbCharacterRepository : ICharacterRepository
    {
        private readonly StarWarsContext _context;

        public DbCharacterRepository(StarWarsContext context)
        {
            _context = context;
        }

        public async Task<Domain.Model.Character> ReadAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Read operation");

            CharacterEntity entity = await GetQueryable().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return entity?.MapToCharacter();
        }

        public async Task<Paged<Domain.Model.Character>> SearchAsync(CharacterSearchQuery query)
        {
            if (query == null)
                throw HttpError.BadRequest("Search action must have query");

            query.Validate();

            IQueryable<CharacterEntity> queryable = GetQueryable().AsNoTracking();

            if (!string.IsNullOrEmpty(query.Name))
                queryable = queryable.Where(x => x.Name == query.Name);

            if (!string.IsNullOrEmpty(query.Planet))
                queryable = queryable.Where(x => x.Planet == query.Planet);


            return await queryable.Paginate(query, entity => entity.MapToCharacter());
        }

        public async Task<Domain.Model.Character> UpdateAsync(Domain.Model.Character character)
        {
            if (character == null)
                throw HttpError.BadRequest("Character must not be null.");

            if (string.IsNullOrEmpty(character.Id))
                throw HttpError.BadRequest("Character.id must not be null.");


            CharacterEntity entity = await GetQueryable().FirstOrDefaultAsync(x => x.Id == character.Id);
            if (entity == null)
                throw HttpError.NotFound($"Character (id: {character.Id}) not found");


            string[] modelEpisodesIds = character.EpisodesIds.ToArray();

            if (await _context.Episodes.Where(x => modelEpisodesIds.Contains(x.Id)).CountAsync() !=
                modelEpisodesIds.Length)
                throw HttpError.BadRequest("Character.EpisodesIds must exist in system.");

            string[] modelFriendsIds = character.FriendsIds.ToArray();
            if (await _context.Characters.Where(x => modelFriendsIds.Contains(x.Id)).CountAsync() !=
                modelFriendsIds.Length)
                throw HttpError.BadRequest("Character.FriendsIds must exist in system.");

            entity.Name = character.Name;
            entity.Planet = character.Planet;

            //remove missing episodes
            CharacterEpisodeEntity[] removedEpisodes =
                entity.CharacterEpisodes.Where(x => !modelEpisodesIds.Contains(x.EpisodeId)).ToArray();
            foreach (CharacterEpisodeEntity removedEpisode in removedEpisodes)
                entity.CharacterEpisodes.Remove(removedEpisode);

            //add new episodes
            foreach (string episodeId in modelEpisodesIds
               .Except(entity.CharacterEpisodes.Select(x => x.EpisodeId)))
                entity.CharacterEpisodes.Add(new CharacterEpisodeEntity
                {
                    CharacterId = character.Id, EpisodeId = episodeId
                });


            CharacterFriendEntity[] entityFriends = entity.Friends.Union(entity.FriendOf).ToArray();

            //remove missing friendshps
            _context.CharacterFriends.RemoveRange(entityFriends.Where(x =>
                !modelFriendsIds.Contains(x.CharacterId == entity.Id ? x.FriendId : x.CharacterId)));

            //add new friendships
            foreach (string newFriendId in modelFriendsIds.Except(entityFriends.Select(x =>
                x.CharacterId == entity.Id ? x.FriendId : x.CharacterId)))
                _context.CharacterFriends.Add(new CharacterFriendEntity
                    {CharacterId = entity.Id, FriendId = newFriendId });


            EntityEntry<CharacterEntity> result = _context.Characters.Update(entity);
            if (await _context.SaveChangesAsync() > 0)
                return result.Entity.MapToCharacter();


            throw HttpError.InternalServerError("An error occurred during character update.");
        }

        public async Task<bool> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw HttpError.BadRequest("Id must be provided during Delete operation");

            CharacterEntity entity = await GetQueryable().FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw HttpError.NotFound($"Character (id: {id}) not found");


            _context.CharacterFriends.RemoveRange(entity.Friends);
            _context.CharacterFriends.RemoveRange(entity.FriendOf);
            _context.CharacterEpisodes.RemoveRange(entity.CharacterEpisodes);
            _context.Characters.Remove(entity);

            return await _context.SaveChangesAsync() > 0;
        }


        public async Task<Domain.Model.Character> CreateAsync(Domain.Model.Character character)
        {
            if (character == null)
                throw HttpError.BadRequest("Character must not be null.");

            character.Id = Guid.NewGuid().ToString();
            if (character.EpisodesIds?.Any() == true)
                if (await _context.Episodes.CountAsync(x => character.EpisodesIds.Contains(x.Id)) !=
                    character.EpisodesIds.Length)
                    throw HttpError.BadRequest("Character.EpisodesIds must exist in system.");
            if (character.FriendsIds?.Any() == true)
            {
                string[] friendsToBeIds = character.FriendsIds.ToArray();
                int existingFriendsToBeCount = _context.Characters.Count(x => friendsToBeIds.Contains(x.Id));
                if (friendsToBeIds.Length != existingFriendsToBeCount)
                    throw HttpError.BadRequest("Character.FriendsIds must exist in system.");
            }

            CharacterEntity entity = character.MapToCharacterEntity();
            EntityEntry<CharacterEntity> result = await _context.Characters.AddAsync(entity);

            if (await _context.SaveChangesAsync() > 0)
                return result.Entity.MapToCharacter();

            throw HttpError.InternalServerError("An error occurred during character providing.");
        }

        private IQueryable<CharacterEntity> GetQueryable() => _context.Characters
           .Include(x => x.Friends)
           .Include(x => x.FriendOf)
           .Include(x => x.CharacterEpisodes);
    }
}