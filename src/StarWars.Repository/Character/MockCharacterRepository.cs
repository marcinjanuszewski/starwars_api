﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StarWars.Core.Error;
using StarWars.Domain.Pagination;
using StarWars.Domain.Query;
using StarWars.Repository.Extensions;

namespace StarWars.Repository.Character
{
    public class MockCharacterRepository : ICharacterRepository
    {
        private static readonly Domain.Model.Character[] _characters =
        {
            new Domain.Model.Character
            {
                Id = "9a3e726b-6aa1-493a-a35e-4bbe44cc4ba7",
                Name = "Luke Skywalker",
                FriendsIds = new[] {"e4a0d7fa-c786-4a0c-a648-4449325b93f8"},
                EpisodesIds = new[]
                {
                    "b0f4f0b3-b1ea-4bd6-9510-6ec2b78e2aaa",
                    "e4ec115e-080a-41a7-aaa8-2200b3a95b28",
                    "e17e772f-62c4-471c-8c0d-71e9a7801445"
                }
            },
            new Domain.Model.Character
            {
                Id = "e4a0d7fa-c786-4a0c-a648-4449325b93f8",
                Name = "Han Solo",
                FriendsIds = new[] {"9a3e726b-6aa1-493a-a35e-4bbe44cc4ba7"},
                EpisodesIds = new[]
                {
                    "b0f4f0b3-b1ea-4bd6-9510-6ec2b78e2aaa",
                    "e4ec115e-080a-41a7-aaa8-2200b3a95b28",
                    "e17e772f-62c4-471c-8c0d-71e9a7801445"
                }
            },
            new Domain.Model.Character
            {
                Id = "593f603f-f271-4ff2-bda2-ff1aa44270b3",
                Name = "Darth Vader",
                FriendsIds = new[] {"d84253eb-6852-415c-98aa-d250e40f1b4b"},
                EpisodesIds = new[]
                {
                    "b0f4f0b3-b1ea-4bd6-9510-6ec2b78e2aaa",
                    "e4ec115e-080a-41a7-aaa8-2200b3a95b28",
                    "e17e772f-62c4-471c-8c0d-71e9a7801445"
                }
            },
            new Domain.Model.Character
            {
                Id = "d84253eb-6852-415c-98aa-d250e40f1b4b",
                Name = "Wilhuff Tarkin",
                FriendsIds = new[] {"593f603f-f271-4ff2-bda2-ff1aa44270b3"},
                EpisodesIds = new[] {"e17e772f-62c4-471c-8c0d-71e9a7801445"}
            }
        };

        public Task<Domain.Model.Character> CreateAsync(Domain.Model.Character episode) =>
            throw HttpError.InternalServerError("Not implemented.");

        public Task<Domain.Model.Character> ReadAsync(string id)
        {
            return Task.FromResult(_characters.FirstOrDefault(x => x.Id == id));
        }

        public Task<Domain.Model.Character> UpdateAsync(Domain.Model.Character character) =>
            throw HttpError.InternalServerError("Not implemented.");

        public Task<bool> DeleteAsync(string id) => throw HttpError.InternalServerError("Not implemented.");

        public Task<Paged<Domain.Model.Character>> SearchAsync(CharacterSearchQuery query)
        {
            IEnumerable<Domain.Model.Character> characters = _characters;
            if (!string.IsNullOrEmpty(query.Name))
                characters = characters.Where(x => x.Name == query.Name);

            if (!string.IsNullOrEmpty(query.Planet))
                characters = characters.Where(x => x.Planet == query.Planet);

            Domain.Model.Character[] filteredCharacters = characters.ToArray();

            return Task.FromResult(filteredCharacters.Paginate(query));
        }
    }
}