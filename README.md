# StarWars
Example CRUD


## Dev setup

This app is fully dockerized! To use it you have to have docker and docker-compose installed. Just run command: `docker-compose -p starwars up` and hack!

App default port is 33000. Swagger is available on: http://localhost:33000/swagger