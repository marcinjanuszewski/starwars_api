﻿using System.Net;
using NUnit.Framework;
using StarWars.Core.Error;

namespace StarWars.Tests
{
    public static class AssertHelper
    {
        public static void AssertHttpErrorExceptionWithStatus(AsyncTestDelegate testDelegate, HttpStatusCode statusCode)
        {
            var exception = Assert.CatchAsync<HttpErrorException>(testDelegate);
            Assert.True(exception.StatusCode == statusCode);
        }
    }
}