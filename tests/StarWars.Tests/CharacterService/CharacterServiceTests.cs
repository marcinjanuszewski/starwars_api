﻿using System.Net;
using System.Threading.Tasks;
using NUnit.Framework;
using StarWars.Domain.Model;
using StarWars.Repository.Character;

namespace StarWars.Tests.CharacterService
{
    [TestFixture]
    public class CharacterServiceTests
    {
        private Services.Character.CharacterService _characterService;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var mockRepository = new MockCharacterRepository();
            _characterService = new Services.Character.CharacterService(mockRepository);
        }


        [TestCase("9a3e726b-6aa1-493a-a35e-4bbe44cc4ba7", true)]
        [TestCase("29e026ac-c4e7-45e8-b156-539202360f28", false)]
        public async Task Read_ShouldPass(string id, bool shouldExist)
        {
            Character resource = await _characterService.ReadAsync(id);
            Assert.AreEqual(resource != null, shouldExist);
        }


        [TestCase]
        public void Read_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _characterService.ReadAsync(null),
                HttpStatusCode.BadRequest);
        }

        [TestCase]
        public async Task Create_NullCharacter_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _characterService.CreateAsync(null),
                HttpStatusCode.BadRequest);
        }


        [TestCase]
        public async Task Update_NullCharacter_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _characterService.UpdateAsync(null),
                HttpStatusCode.BadRequest);
        }

        [TestCase]
        public async Task Update_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(
                async () => await _characterService.UpdateAsync(new Character()),
                HttpStatusCode.BadRequest);
        }


        [TestCase]
        public async Task Delete_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _characterService.DeleteAsync(null),
                HttpStatusCode.BadRequest);
        }
    }
}