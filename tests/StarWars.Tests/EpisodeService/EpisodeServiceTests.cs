﻿using System.Net;
using System.Threading.Tasks;
using NUnit.Framework;
using StarWars.Domain.Model;
using StarWars.Repository.Episode;

namespace StarWars.Tests.EpisodeService
{
    [TestFixture]
    public class EpisodeServiceTests
    {
        private Services.Episode.EpisodeService _episodeService;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var mockRepository = new MockEpisodeRepository();
            _episodeService = new Services.Episode.EpisodeService(mockRepository);
        }


        [TestCase("b0f4f0b3-b1ea-4bd6-9510-6ec2b78e2aaa", true)]
        [TestCase("f35e29e9-e29c-400b-b989-2cdf4ab0b0c9", false)]
        public async Task Read_ShouldPass(string id, bool shouldExist)
        {
            Episode resource = await _episodeService.ReadAsync(id);
            Assert.AreEqual(resource != null, shouldExist);
        }

        [TestCase]
        public void Read_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _episodeService.ReadAsync(null),
                HttpStatusCode.BadRequest);
        }


        [TestCase]
        public async Task Create_NullEpisode_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _episodeService.CreateAsync(null),
                HttpStatusCode.BadRequest);
        }


        [TestCase]
        public async Task Update_NullEpisode_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _episodeService.UpdateAsync(null),
                HttpStatusCode.BadRequest);
        }

        [TestCase]
        public async Task Update_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(
                async () => await _episodeService.UpdateAsync(new Episode()),
                HttpStatusCode.BadRequest);
        }


        [TestCase]
        public async Task Delete_NullId_ShouldOccurBadRequest()
        {
            AssertHelper.AssertHttpErrorExceptionWithStatus(async () => await _episodeService.DeleteAsync(null),
                HttpStatusCode.BadRequest);
        }
    }
}